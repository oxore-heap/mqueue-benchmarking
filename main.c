#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <mqueue.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define QUEUE_BASENAME "/mq"

#define NSEC_PER_SEC (1000 * 1000 * 1000)

// 128 bytes message
#define MSG "0000000000000000" "1111111111111111" \
            "2222222222222222" "3333333333333333" \
            "4444444444444444" "5555555555555555" \
            "6666666666666666" "777777777777777"

#define MESSAGES_PER_TEST 10000000
#define THREADS_COUNT 2

struct common_data {
    mqd_t mq[THREADS_COUNT];
    size_t counter_rx[THREADS_COUNT];
    size_t counter_misread[THREADS_COUNT];
    size_t counter_tx[THREADS_COUNT];
    size_t counter_miswrite[THREADS_COUNT];
};

struct thread_arg {
    size_t id;
    struct common_data *common;
};

static void *party(void *arg)
{
    struct thread_arg *targ = arg;
    struct common_data *data = targ->common;
    const struct timespec timeout = {0};
    struct timespec start_time, end_time;
    char msg[1000];
    unsigned int prio;
    int ret;

    clock_gettime(CLOCK_MONOTONIC, &start_time);

    while (data->counter_rx[targ->id] < MESSAGES_PER_TEST
            || data->counter_tx[targ->id] < MESSAGES_PER_TEST) {

        if (data->counter_tx[targ->id] < MESSAGES_PER_TEST) {
            ret = mq_timedsend(
                    data->mq[(targ->id+1) % THREADS_COUNT],
                    MSG,
                    sizeof(MSG),
                    0,
                    &timeout);

            if (ret != (mqd_t)-1) {
                data->counter_tx[targ->id]++;
            } else {
                data->counter_miswrite[targ->id]++;
            }
        }

        if (data->counter_rx[targ->id] < MESSAGES_PER_TEST) {
            ret = mq_timedreceive(
                    data->mq[targ->id],
                    msg,
                    sizeof(msg),
                    &prio,
                    &timeout);

            if (ret != (mqd_t)-1) {
                data->counter_rx[targ->id]++;
            } else {
                data->counter_misread[targ->id]++;
            }
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &end_time);

    time_t tv_sec, tv_nsec;

    if ((end_time.tv_nsec - start_time.tv_nsec) < 0) {
        tv_sec = end_time.tv_sec - start_time.tv_sec - 1;
        tv_nsec = end_time.tv_nsec - start_time.tv_nsec + NSEC_PER_SEC;
    } else {
        tv_sec = end_time.tv_sec - start_time.tv_sec;
        tv_nsec = end_time.tv_nsec - start_time.tv_nsec;
    }

    struct timespec duration_time = {
        .tv_sec = tv_sec,
        .tv_nsec = tv_nsec,
    };

    printf(
            "party%zu: %lld.%06ds\n",
            targ->id,
            (long long)duration_time.tv_sec,
            (int)(duration_time.tv_nsec / 10e3));

    return NULL;
}

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    int ret;
    pthread_t party1_pt = {0}, party2_pt = {0};
    pthread_attr_t attr = {0};

    ret = pthread_attr_init(&attr);
    assert(ret == 0);

    struct mq_attr mq_attr = {
        .mq_maxmsg = 10,
        .mq_msgsize = 128,
    };

    struct common_data data = {0};

    for (size_t i = 0; i < THREADS_COUNT; i++) {
        char name[1024] = {0};
        sprintf(name, QUEUE_BASENAME "%zu", i);
        data.mq[i] = mq_open(name, O_CREAT | O_RDWR, 0666, &mq_attr);
        if (data.mq[i] == (mqd_t)-1) {
            printf("mq_open(%zu): %s\n", i, strerror(errno));
            goto close_mq;
        }
    }

    struct thread_arg arg1 = {
        .id = 0,
        .common = &data,
    };
    struct thread_arg arg2 = {
        .id = 1,
        .common = &data,
    };

    pthread_create(&party1_pt, &attr, party, &arg1);
    pthread_create(&party2_pt, &attr, party, &arg2);
    pthread_join(party1_pt, NULL);
    pthread_join(party2_pt, NULL);

close_mq:
    for (size_t i = 0; i < THREADS_COUNT; i++) {
        printf("counter_tx[%zu] = %zu\n", i, data.counter_tx[i]);
        printf("counter_rx[%zu] = %zu\n", i, data.counter_rx[i]);
        printf("counter_misread[%zu] = %zu\n", i, data.counter_misread[i]);
        printf("counter_miswrite[%zu] = %zu\n", i, data.counter_miswrite[i]);
    }

    for (size_t i = 0; i < THREADS_COUNT; i++) {
        if (data.mq[i] != (mqd_t)0) {
            char name[1024] = {0};
            sprintf(name, QUEUE_BASENAME "%zu", i);
            mq_close(data.mq[i]);
            mq_unlink(name);
        }
    }

    ret = pthread_attr_destroy(&attr);
    assert(ret == 0);
}
